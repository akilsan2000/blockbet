// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// You can also run a script with `npx hardhat run <script>`. If you do that, Hardhat
// will compile your contracts, add the Hardhat Runtime Environment's members to the
// global scope, and execute the script.
const { network, run } = require("hardhat")
const { deployAutomationCounter } = require("./deployAutomationCounter")

const { deployApiConsumer } = require("./deployApiConsumer")
const { deployAutomationCounterV2 } = require("./deployAutomationCounterV2")
async function main() {
    await run("compile")
    const chainId = network.config.chainId
    // await deployApiConsumer(11155111)
    await deployAutomationCounter(11155111)
    // await deployAutomationCounterV2(11155111)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
    console.error(error)
    process.exitCode = 1
})
