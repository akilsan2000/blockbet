const { ethers, network, run } = require("hardhat")
const {
    VERIFICATION_BLOCK_CONFIRMATIONS,
    networkConfig,
    developmentChains,
} = require("../../helper-hardhat-config")

async function deployAutomationCounterV2(chainId) {
    // const automationUpdateInterval = networkConfig[chainId]["automationUpdateInterval"]
    oracleAddress = networkConfig[chainId]["oracle"]
    linkTokenAddress = networkConfig[chainId]["linkToken"]
    const jobId = ethers.utils.toUtf8Bytes(networkConfig[chainId]["jobId"])
    const fee = networkConfig[chainId]["fee"]
    const automationCounterV2Factory = await ethers.getContractFactory("AutomationCounterV2")
    const automationCounterV2 = await automationCounterV2Factory.deploy(oracleAddress, jobId, fee, linkTokenAddress)

    const waitBlockConfirmations = developmentChains.includes(network.name)
        ? 1
        : VERIFICATION_BLOCK_CONFIRMATIONS
    await automationCounterV2.deployTransaction.wait(waitBlockConfirmations)

    console.log(`Automation Counter V2 deployed to ${automationCounterV2.address} on ${network.name}`)

    // if (!developmentChains.includes(network.name) && process.env.ETHERSCAN_API_KEY) {
    //     await run("verify:verify", {
    //         address: automationCounterV2.address,
    //         constructorArguments: [oracleAddress, jobId, fee, linkTokenAddress],
    //     })
    // }
}

module.exports = {
    deployAutomationCounterV2,
}
