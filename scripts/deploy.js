const { ethers } = require("hardhat");

async function main() {
  const bettingContract = await ethers.deployContract("BettingContract");
  
  console.log("BettingContract deployed to:", await bettingContract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });