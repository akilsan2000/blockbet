const {
    loadFixture,
  } = require("@nomicfoundation/hardhat-network-helpers");
const { expect } = require("chai");
const { network, ethers } = require("hardhat")

describe("BettingContract", function () {
    const lowBetValue = "0.001";
    const middleBetValue = "0.005";
    const highBetValue = "0.01";

    async function deployBettingContractFixture() {
        const [owner, addr1, addr2, addr3] = await ethers.getSigners();
        const bettingContract = await ethers.deployContract("BettingContract");
        return { bettingContract, owner, addr1, addr2, addr3 };
    }

    it("should create a bet", async function () {
      const { bettingContract } = await loadFixture(deployBettingContractFixture);
      const initialBetsCount = await bettingContract.getBetsCount();
      const chosenTeam = 0; // 1 for Team.HOME, 2 for Team.AWY, 3 for Team.DRAW
      const gameId = 1;
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(lowBetValue) });
      const newBetsCount = await bettingContract.getBetsCount();
      expect(newBetsCount).to.equal(initialBetsCount + BigInt(1));
    });

    it('should allow players to join a bet', async function () {
      const { bettingContract, owner, addr1 } = await loadFixture(deployBettingContractFixture);
      const chosenTeam = 2; // Team.AWAY
      const gameId = 1;
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(lowBetValue) }); // create
      await bettingContract.connect(addr1).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(lowBetValue) }); // join

      const bets = await bettingContract.getBetsByGameId(gameId);
      const bet = bets[0];
      const playerIdx = bet.players.indexOf(addr1.address);

      expect(bet.players).to.deep.equal([owner.address, addr1.address]);
      expect(bet.chosenTeams[playerIdx]).to.equal(1);
    });

    it('should allow players to join a bet on the same game with different amounts', async function () {
      const { bettingContract, owner, addr1, addr2 } = await loadFixture(deployBettingContractFixture);
      const chosenTeam = 2; // Team.AWAY
      const gameId = 1;
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(lowBetValue) }); // create
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(middleBetValue) }); // create
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(highBetValue) }); // create
      await bettingContract.connect(addr1).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(middleBetValue) }); // join

      const bets = await bettingContract.getBetsByGameId(gameId);
      const bet = bets[1];
      const playerIdx = bet.players.indexOf(addr1.address);

      expect(bet.players).to.deep.equal([owner.address, addr1.address]);
      expect(bet.chosenTeams[playerIdx]).to.equal(1);
    });

    it('should fail if a player with the same address has already joined a bet', async function () {
      const { bettingContract, addr1 } = await loadFixture(deployBettingContractFixture);
      const chosenTeam = 2; // Team.AWAY
      const gameId = 1;

      // First player joins the bet
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(lowBetValue) }); // create

      // Second player joins the bet
      await bettingContract.connect(addr1).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(lowBetValue) }); // join

      // Attempt for the second player to join the bet again with the same address
      await expect(
          bettingContract.connect(addr1).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(lowBetValue) }) // join (fail)
      ).to.be.revertedWith("Player has already joined the bet");
    });

    it('should set playerHasAlreadyJoined true if player has already joined', async function () {
      const { bettingContract, owner, addr1, addr2 } = await loadFixture(deployBettingContractFixture);
      const chosenTeam = 2; // Team.AWAY
      const gameId = 1;
      await bettingContract.createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(lowBetValue) }); // create
      await bettingContract.connect(addr1).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(lowBetValue) }); // join

      const bets = await bettingContract.connect(addr1).getBetsByGameId(gameId); // is in players array
      const bets2 = await bettingContract.connect(addr2).getBetsByGameId(gameId); // is not in players array
      await expect(bets[0].playerHasAlreadyJoined).to.equal(true);
      await expect(bets2[0].playerHasAlreadyJoined).to.equal(false);
    });

    it('should finish a bet and distribute winnings correctly', async function () {
      const { bettingContract, owner, addr1, addr2, addr3 } = await loadFixture(deployBettingContractFixture);
      const chosenTeam = 2; // Team.AWAY
      const gameId = 1;

      // Create and join bets
      await bettingContract.connect(addr1).createOrJoinBet(gameId, chosenTeam, { value: ethers.utils.parseEther(middleBetValue) }); // create
      await bettingContract.connect(addr2).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(middleBetValue) }); // join
      await bettingContract.connect(addr3).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther(middleBetValue) }); // join

      // Game result (winning team = Team.HOME) for finsihed game
      const result = 1;

      // Get balances before finishing the bet
      const addr1BalanceBefore = await ethers.provider.getBalance(addr1.address);
      const addr2BalanceBefore = await ethers.provider.getBalance(addr2.address);
      const addr3BalanceBefore = await ethers.provider.getBalance(addr3.address);

      // Finish bet
      await bettingContract.finishBet(gameId, result);

      const bets = await bettingContract.getBetsByGameId(gameId);
      expect(bets[1].isFinished).to.equal(true);

      // Get balances after finishing the bet
      const addr1BalanceAfter = await ethers.provider.getBalance(addr1.address);
      const addr2BalanceAfter = await ethers.provider.getBalance(addr2.address);
      const addr3BalanceAfter = await ethers.provider.getBalance(addr3.address);

      // Check if winners are determined correctly
      const totalWinnings = ethers.utils.parseEther(middleBetValue) * 3; // Total bet amount for winners
      const individualWinnings = totalWinnings / 2; // Individual bet amount for winners
      expect(addr1BalanceAfter.sub(addr1BalanceBefore)).to.equal(0); // addr1 should not receive any winnings
      expect(addr2BalanceAfter.sub(addr2BalanceBefore)).to.equal(individualWinnings); // addr2 should receive half of the total amount
      expect(addr3BalanceAfter.sub(addr3BalanceBefore)).to.equal(individualWinnings); // addr2 should receive half of the total amount
    });

    // it('should handle "No winners to distribute winnings" case', async function () {
    //   const { bettingContract } = await loadFixture(deployBettingContractFixture);
    //   const gameId = 1;
    //   const result = 1; // Team.HOME

    //   await bettingContract.createOrJoinBet(gameId, 2, { value: ethers.utils.parseEther(lowBetValue) }); // create
    //   await bettingContract.createOrJoinBet(gameId, 3, { value: ethers.utils.parseEther(middleBetValue) }); // create
    //   await expect(
    //     bettingContract.finishBet(gameId, result)
    //   ).to.be.revertedWith("No winners to distribute winnings");
    // });

    it('should prevent reentrancy in finishBet function', async function () {
      const { bettingContract, addr2 } = await loadFixture(deployBettingContractFixture);
      const gameId = 1;
      const result = 1; // Team.HOME

      // Create and join bets
      await bettingContract.createOrJoinBet(gameId, 2, { value: ethers.utils.parseEther('0.001') }); // create
      await bettingContract.createOrJoinBet(gameId, 3, { value: ethers.utils.parseEther('0.005') }); // create
      await bettingContract.connect(addr2).createOrJoinBet(gameId, 1, { value: ethers.utils.parseEther('0.005') }); // join

      // First call to finishBet
      await bettingContract.finishBet(gameId, result);

      // Second call to finishBet should revert
      await expect(
        bettingContract.finishBet(gameId, result)
      ).to.be.revertedWith("Bet is already finished");
    });
});
