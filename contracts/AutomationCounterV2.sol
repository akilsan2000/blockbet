// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

// AutomationCompatible.sol imports the functions from both ./AutomationBase.sol and
// ./interfaces/AutomationCompatibleInterface.sol
import "@chainlink/contracts/src/v0.8/AutomationCompatible.sol";
import "@chainlink/contracts/src/v0.8/ChainlinkClient.sol";
import "hardhat/console.sol";

interface APIConsumerInterface{
  function requestVolumeData() external returns (bytes32 requestId);
}
contract AutomationCounterV2 is AutomationCompatibleInterface, ChainlinkClient {
    using Chainlink for Chainlink.Request;
    address public constant apiContractAddress = 0x90a39A6f7e12BEf818BB03C2DE1035305f743d9A;
    APIConsumerInterface ApiContract = APIConsumerInterface(apiContractAddress);
   
    bool public isFinished;
    address private oracle;
    bytes32 private jobId;
    uint256 private fee;

    /**
     * Use an interval in seconds and a timestamp to slow execution of Upkeep
     */
    uint256 public immutable interval = 480;
    uint256 public lastTimeStamp;

    constructor(address _oracle, bytes32 _jobId, uint256 _fee, address _link) {
        if (_link == address(0)) {
            setPublicChainlinkToken();
        } else {
            setChainlinkToken(_link);
        }
        oracle = _oracle;
        jobId = _jobId;
        fee = _fee;
        lastTimeStamp = block.timestamp;
    }

    function checkUpkeep(
        bytes memory /* checkData */
    )
        public
        override
        returns (
            bool upkeepNeeded,
            bytes memory /* performData */
        )
    {
        if ((block.timestamp - lastTimeStamp) > interval){
            requestVolumeData();
        }
        upkeepNeeded = isFinished;
    }

    function requestResults() public {
        ApiContract.requestVolumeData();
    }

    function performUpkeep(
        bytes calldata /* performData */
    ) external override {
        (bool upkeepNeeded, ) = checkUpkeep("");
        require(upkeepNeeded, "Time interval not met or Game is not finished");
        requestResults();
        lastTimeStamp = block.timestamp;
    }

    /**
     * @notice Creates a Chainlink request to retrieve API response, find the target
     *
     * @return requestId - id of the request
     */
    function requestVolumeData() public returns (bytes32 requestId) {
        Chainlink.Request memory request = buildChainlinkRequest(
            jobId,
            address(this),
            this.fulfill.selector
        );

        // Set the URL to perform the GET request on
        request.add(
            "get",
            "https://65788dc3f08799dc80459625.mockapi.io/score-games"
        );

        request.add("path", "0,isFinished"); 

        // Sends the request
        return sendChainlinkRequestTo(oracle, request, fee);
    }

    /**
     * @notice Receives the response in the form of uint256
     *
     * @param _requestId - id of the request
     * @param _isFinished - response;
     */
    function fulfill(
        bytes32 _requestId,
        bool _isFinished
    ) public recordChainlinkFulfillment(_requestId) {
        isFinished = _isFinished;
    }
}