// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

// AutomationCompatible.sol imports the functions from both ./AutomationBase.sol and
// ./interfaces/AutomationCompatibleInterface.sol
import "@chainlink/contracts/src/v0.8/AutomationCompatible.sol";
import "@chainlink/contracts/src/v0.8/ChainlinkClient.sol";
import "hardhat/console.sol";

//Interface name is not important, however functions in it are important
interface APIConsumerInterface{
  function requestVolumeData() external returns (bytes32 requestId);
}

contract AutomationCounter is AutomationCompatibleInterface, ChainlinkClient {
    using Chainlink for Chainlink.Request;
    address public constant apiContractAddress = 0x15A4EAbA080cF736A60d8C8A29eeaA582c555C9E;
    APIConsumerInterface ApiContract = APIConsumerInterface(apiContractAddress);
    /**
     * Public counter variable
     */
    bool public isFinished;

    /**
     * Use an interval in seconds and a timestamp to slow execution of Upkeep
     */
    uint256 public immutable interval = 600;
    uint256 public lastTimeStamp;

    constructor() {
        lastTimeStamp = block.timestamp;
    }

    function checkUpkeep(
        bytes memory /* checkData */
    )
        public
        view
        override
        returns (
            bool upkeepNeeded,
            bytes memory /* performData */
        )
    {
        upkeepNeeded = (block.timestamp - lastTimeStamp) > interval;
    }

    function requestResults() public {
        ApiContract.requestVolumeData();
    }

    function performUpkeep(
        bytes calldata /* performData */
    ) external override {
        (bool upkeepNeeded, ) = checkUpkeep("");
        require(upkeepNeeded, "Time interval not met");
        requestResults();
        lastTimeStamp = block.timestamp;
    }
}