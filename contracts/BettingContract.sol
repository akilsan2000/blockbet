// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.9;

import "hardhat/console.sol";

contract BettingContract {
    enum Team { NONE, HOME, AWAY, DRAW }

    struct BetInfo {
        uint256 gameId;
        address creator;
        address[] players;
        Team[] chosenTeams;
        uint256 betAmount;
        bool isFinished;
        bool playerHasAlreadyJoined;
    }


    struct Bet {
        uint256 gameId;
        address creator;
        address[] players;
        mapping(address => Team) chosenTeams;
        uint256 betAmount;
        bool isFinished;
    }

    Bet[] public bets;
    mapping(bytes32 => uint256) public betIndexByGameIdAndBetAmount;
    mapping(uint256 => bool) public isBetFinished;

    event BetFinished(uint256 indexed gameId, address[] winners, uint256 totalWinningAmount);

    modifier nonReentrant(uint256 gameId) {
        require(!isBetFinished[gameId], "Bet is already finished");
        isBetFinished[gameId] = true;
        _;
        isBetFinished[gameId] = false;
    }

    function createOrJoinBet(uint256 gameId, Team creatorTeam) external payable {
        require(msg.value > 0, "Bet amount must be greater than 0");
        require(gameId > 0, "Game ID must be greater than 0");

        bytes32 compositeKey = keccak256(abi.encodePacked(gameId, msg.value));
        uint256 betIndex = betIndexByGameIdAndBetAmount[compositeKey];

        if (betIndex == 0) {
            // Create a new bet
            bets.push();
            betIndex = bets.length;
            Bet storage newBet = bets[betIndex-1];
            newBet.gameId = gameId;
            newBet.creator = msg.sender;
            newBet.betAmount = msg.value;
            newBet.players.push(msg.sender);
            newBet.chosenTeams[msg.sender] = creatorTeam;
            betIndexByGameIdAndBetAmount[compositeKey] = betIndex;
        } else {
            // Join an existing bet
            require(!bets[betIndex-1].isFinished, "Bet is already finished");
            require(bets[betIndex-1].chosenTeams[msg.sender] == Team.NONE, "Player has already joined the bet");

            bets[betIndex-1].players.push(msg.sender);
            bets[betIndex-1].chosenTeams[msg.sender] = creatorTeam;
        }
    }

    function getBetsCount() public view returns(uint) {
        return bets.length;
    }

    function getBetsByGameId(uint256 gameId) public view returns (BetInfo[] memory) {
        uint256 lowBetAmount = 0.001 ether;
        bytes32 lowBetCompositeKey = keccak256(abi.encodePacked(gameId, lowBetAmount));
        uint256 lowBetIndex = betIndexByGameIdAndBetAmount[lowBetCompositeKey];

        uint256 middleBetAmount = 0.005 ether;
        bytes32 middleBetCompositeKey = keccak256(abi.encodePacked(gameId, middleBetAmount));
        uint256 middleBetIndex = betIndexByGameIdAndBetAmount[middleBetCompositeKey];

        uint256 highBetAmount = 0.01 ether;
        bytes32 highBetCompositeKey = keccak256(abi.encodePacked(gameId, highBetAmount));
        uint256 highBetIndex = betIndexByGameIdAndBetAmount[highBetCompositeKey];

        BetInfo[] memory betInfos = new BetInfo[](3);

        if (lowBetIndex > 0) {
            betInfos[0] = getBetInfoByGameId(lowBetIndex);
        }

        if (middleBetIndex > 0) {
            betInfos[1] = getBetInfoByGameId(middleBetIndex);
        }

        if (highBetIndex > 0) {
            betInfos[2] = getBetInfoByGameId(highBetIndex);
        }

        return betInfos;
    }

    function getBetInfoByGameId(uint256 betIndex) internal view returns (BetInfo memory){
        BetInfo memory betInfo;
        betInfo.gameId = bets[betIndex-1].gameId;
        betInfo.creator = bets[betIndex-1].creator;
        betInfo.betAmount = bets[betIndex-1].betAmount;
        betInfo.isFinished = bets[betIndex-1].isFinished;
        betInfo.chosenTeams = new Team[](bets[betIndex-1].players.length);
        betInfo.players = new address[](bets[betIndex-1].players.length);
        betInfo.playerHasAlreadyJoined = false;

        for (uint256 j = 0; j < bets[betIndex-1].players.length; j++) {
            if(bets[betIndex-1].players[j] == msg.sender){
                betInfo.playerHasAlreadyJoined = true;
            }
            betInfo.chosenTeams[j] = bets[betIndex-1].chosenTeams[bets[betIndex-1].players[j]];
            betInfo.players[j] = bets[betIndex-1].players[j];
        }
        return betInfo;
    }

    function finishBet(uint256 gameId, Team result) external nonReentrant(gameId) {
        finishBetForAmount(gameId, result, 0.001 ether);
        finishBetForAmount(gameId, result, 0.005 ether);
        finishBetForAmount(gameId, result, 0.01 ether);
    }

    function finishBetForAmount(uint256 gameId, Team result, uint256 betAmount) internal {
        bytes32 compositeKey = keccak256(abi.encodePacked(gameId, betAmount));
        uint256 betIndex = betIndexByGameIdAndBetAmount[compositeKey];
        if (betIndex > 0) {
            require(!bets[betIndex-1].isFinished, "Bet is already finished");
            address[] memory tempWinners = new address[](bets[betIndex-1].players.length);
            uint256 totalWinningAmount = bets[betIndex-1].betAmount * bets[betIndex-1].players.length;

            bets[betIndex-1].isFinished = true;

            // determine winners
            uint256 tempWinnersCount = 0;
            for (uint256 j = 0; j < bets[betIndex-1].players.length; j++) {
                address player = bets[betIndex-1].players[j];
                Team chosenTeam = bets[betIndex-1].chosenTeams[player];

                if (chosenTeam == result) {
                    tempWinners[tempWinnersCount] = player;
                    tempWinnersCount++;
                }
            }

            address[] memory winners = new address[](tempWinnersCount);
            for (uint256 k = 0; k < tempWinnersCount; k++) {
                winners[k] = tempWinners[k];
            }

            distributeWinnings(winners, totalWinningAmount);
            emit BetFinished(gameId, winners, totalWinningAmount);
        }
    }

    function distributeWinnings(address[] memory winners, uint256 totalWinningAmount) internal {
        if(winners.length > 0){
            uint256 individualWinnings = totalWinningAmount / winners.length;

            for (uint256 i = 0; i < winners.length; i++) {
                payable(winners[i]).transfer(individualWinnings);
            }
        } else {
            console.log("No winners to distribute winnings");
        }
    }
}

