// SPDX-License-Identifier: MIT
pragma solidity ^0.8.7;

import "@chainlink/contracts/src/v0.8/ChainlinkClient.sol";
import "hardhat/console.sol";

enum Team { NONE, HOME, AWAY, DRAW }

interface BettingContractInterface{
  function finishBet(uint256 gameId, Team result) external;
}

contract APIConsumer is ChainlinkClient {
    using Chainlink for Chainlink.Request;
    address public constant bettingContractAddress = 0xCE3DdCFEE102e5E1aF47469db8649d7f2E7BDf01;
    BettingContractInterface BettingContract = BettingContractInterface(bettingContractAddress);


    bool public isFinished;
    uint256 public gameId;
    uint256 public gameWinner;
    address private immutable oracle;
    bytes32 private immutable jobId;
    uint256 private immutable fee;

    event GameFinished(bool isFinished);
    event GameNotFinished(bool isFinished);
    event GameID(uint256 gameId);
    event GameWinner(uint256 gameWinner);
    event Received(address sender, uint256 value);

    /**
     * @notice Executes once when a contract is created to initialize state variables
     *
     * @param _oracle - address of the specific Chainlink node that a contract makes an API call from
     * @param _jobId - specific job for :_oracle: to run; each job is unique and returns different types of data
     * @param _fee - node operator price per API call / data request
     * @param _link - LINK token address on the corresponding network
     *
     * Network: Sepolia
     * Oracle: 0x6090149792dAAeE9D1D568c9f9a6F6B46AA29eFD
     * Job ID: ca98366cc7314957b8c012c72f05aeeb
     * Fee: 0.1 LINK
     */
    constructor(address _oracle, bytes32 _jobId, uint256 _fee, address _link) {
        if (_link == address(0)) {
            setPublicChainlinkToken();
        } else {
            setChainlinkToken(_link);
        }
        oracle = _oracle;
        jobId = _jobId;
        fee = _fee;
    }

    function requestVolumeData() public returns (bytes32 requestId) {
        Chainlink.Request memory request = buildChainlinkRequest(
            jobId,
            address(this),
            this.fulfill.selector
        );

        // Set the URL to perform the GET request on
        request.add(
            "get",
            "https://65788dc3f08799dc80459625.mockapi.io/score-games"
        );

        request.add("path", "0,isFinished");

        return sendChainlinkRequestTo(oracle, request, fee);
    }

    /**
     * @notice Receives the response in the form of uint256
     *
     * @param _requestId - id of the request
     * @param _volume - response;
     */
    function fulfill(
        bytes32 _requestId,
        bool _volume
    ) public recordChainlinkFulfillment(_requestId) {
        isFinished = _volume;
        if (isFinished){
            emit GameFinished(isFinished);
            requestGameId();
        } else {
            emit GameNotFinished(isFinished);
        }
    }

    function requestGameId() public returns (bytes32 requestId) {
        Chainlink.Request memory request = buildChainlinkRequest(
            "ca98366cc7314957b8c012c72f05aeeb",
            address(this),
            this.fulfillGameId.selector
        );

        // Set the URL to perform the GET request on
        request.add(
            "get",
            "https://65788dc3f08799dc80459625.mockapi.io/score-games"
        );

        request.add("path", "0,gameId");

        int256 timesAmount = 10;
        request.addInt("times", timesAmount);

        return sendChainlinkRequestTo(oracle, request, fee);
    }

    /**
     * @notice Receives the response in the form of uint256
     *
     * @param _requestId - id of the request
     * @param _volume - response;
     */
    function fulfillGameId(
        bytes32 _requestId,
        uint256 _volume
    ) public recordChainlinkFulfillment(_requestId) {
        gameId = _volume / 10;
        emit GameID(gameId);
        requestGameWinner();
    }

    function requestGameWinner() public returns (bytes32 requestId) {
        Chainlink.Request memory request = buildChainlinkRequest(
            "ca98366cc7314957b8c012c72f05aeeb",
            address(this),
            this.fulfillGameWinner.selector
        );

        // Set the URL to perform the GET request on
        request.add(
            "get",
            "https://65788dc3f08799dc80459625.mockapi.io/score-games"
        );

        request.add("path", "0,winner");

        int256 timesAmount = 10; // value is 2.0
        request.addInt("times", timesAmount);

        return sendChainlinkRequestTo(oracle, request, fee);
    }

    /**
     * @notice Receives the response in the form of uint256
     *
     * @param _requestId - id of the request
     * @param _volume - response;
     */
    function fulfillGameWinner(
        bytes32 _requestId,
        uint256 _volume
    ) public recordChainlinkFulfillment(_requestId) {
        gameWinner = _volume / 10;
        emit GameWinner(gameWinner);
        callFinishBets();
        
    }
    
    function callFinishBets() public {
        if (gameWinner == 0){
            BettingContract.finishBet(gameId, Team.NONE);
        } else if (gameWinner == 1){
            BettingContract.finishBet(gameId, Team.HOME);
        } else if (gameWinner == 2){
            BettingContract.finishBet(gameId, Team.AWAY);
        } else if (gameWinner == 3){
            BettingContract.finishBet(gameId, Team.DRAW);
        }
    }

    function testBettings() public {
        BettingContract.finishBet(3, Team.AWAY);
    }

    function withdrawLink() external {}

    receive() external payable {
        emit Received(msg.sender, msg.value);
    }
}
