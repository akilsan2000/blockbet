import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Link, Routes } from 'react-router-dom';
import Home from './components/Home';
import ConnectWallet from './components/ConnectWallet';
import './App.css';
import Web3 from 'web3';
import { ethers } from 'ethers';
import { contractAbi, contractAddress } from './utils/constants';
import LayoutBar from './layout/appBar';

function App() {
  const [contract, setContract] = useState(null);
  const [account, setAccount] = useState(null);
  const [provider, setProvider] = useState(null);

  useEffect(() => {
    const provider = new ethers.providers.JsonRpcProvider(process.env.REACT_APP_SEPOLIA_RPC_URL);
    const bettingContract = new ethers.Contract(contractAddress, contractAbi, provider);
    setProvider(provider);
    setContract(bettingContract);
  }, []);

  const handleAccountChange = (value) => {
    setAccount(value);
  };

  return (
    <>
      <LayoutBar></LayoutBar>
      <ConnectWallet onAccountChanged={handleAccountChange}  />
      <Router>
        {/* <nav>
          <ul>
            <li>
              <Link to="/">Home</Link>
            </li>
            <li>
              <Link to="/place-bet">Place Bet</Link>
            </li>
            <li>
              <Link to="/check-results">Check Results</Link>
            </li>
          </ul>
        </nav> */}
          <Routes>
            <Route path="/" element={<Home contract={contract} account={account} />} />
            {/* <Route path="/place-bet" element={<PlaceBet contract={contract} account={account}  />} />
            <Route path="/check-results" element={<CheckResults contract={contract} account={account} />} /> */}
          </Routes>
      </Router>
      <footer style={{display: 'flex', justifyContent: 'center', alignItems: 'center'}}>
        <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
          <p>Made by: </p>
          <div style={{marginBottom: '10px', fontWeight: 'Bold'}}>Akilsan Skanthakumar, Benjamin Anthamatten, Jaris Zurbriggen, Colin Renggli</div>
        </div>
        
      </footer>
    </>
  );
}

export default App;