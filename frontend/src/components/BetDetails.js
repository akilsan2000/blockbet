import React, { useEffect, useState } from 'react';
import { ethers } from 'ethers';
import BetDetailBox from './BetDetailBox';
import { Typography, Button } from '@mui/material';

const BetDetails = ({ selectedGame, betsForSelectedGame, joinBet, account, closeClicked }) => {
    const [chosenTeam, setChosenTeam] = useState(1);

    const lowBetAmount = 0.001; // Low bet amount
    const middleBetAmount = 0.005; // Middle bet amount
    const highBetAmount = 0.01; // High bet amount

    const [lowBetObj, setLowBetObj] = useState(null);
    const [middleBetObj, setMiddleBetObj] = useState(null);
    const [highBetObj, setHighBetObj] = useState(null);

    useEffect(() => {
        console.log('betsForSelectedGame', betsForSelectedGame);
        setLowBetObj(
            betsForSelectedGame.filter(
                (bet) => ethers.BigNumber.from(bet.betAmount).eq(ethers.utils.parseEther(lowBetAmount.toString()))
            )[0]
        );
        setMiddleBetObj(
            betsForSelectedGame.filter(
                (bet) => ethers.BigNumber.from(bet.betAmount).eq(ethers.utils.parseEther(middleBetAmount.toString()))
            )[0]
        );
        setHighBetObj(
            betsForSelectedGame.filter(
                (bet) => ethers.BigNumber.from(bet.betAmount).eq(ethers.utils.parseEther(highBetAmount.toString()))
            )[0]
        );
    }, [betsForSelectedGame]);

    const getCountPlayersByTeam = (bets, selectedTeam) => {
        return bets?.chosenTeams?.reduce((length, playerTeam) => {
            if (playerTeam === selectedTeam) {
                return length + 1;
            }
            return length;
        }, 0);
    }

    return (
        selectedGame &&
        <>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Typography variant="h4">Bets for {selectedGame.teams[0]} vs {selectedGame.teams[1]}</Typography>
                <Button variant="contained" color="grey" onClick={()=>{closeClicked()}}>Close</Button>
            </div>
            <BetDetailBox title={"Low bet"} betAmount={lowBetAmount} betObj={lowBetObj} selectedGame={selectedGame} getCountPlayersByTeam={getCountPlayersByTeam} joinBet={joinBet} account={account} />
            <BetDetailBox title={"Middle bet"} betAmount={middleBetAmount} betObj={middleBetObj} selectedGame={selectedGame} getCountPlayersByTeam={getCountPlayersByTeam} joinBet={joinBet} account={account} />
            <BetDetailBox title={"High bet"} betAmount={highBetAmount} betObj={highBetObj} selectedGame={selectedGame} getCountPlayersByTeam={getCountPlayersByTeam} joinBet={joinBet} account={account} />
        </>
    );
};

export default BetDetails;
