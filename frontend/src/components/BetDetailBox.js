import React, {useState} from 'react';
import { Button, Card, CardContent, Typography, Select, MenuItem} from '@mui/material';

const BetDetailBox = ({ title, betAmount, betObj, selectedGame, getCountPlayersByTeam, joinBet, account }) => {

    const [chosenTeam, setChosenTeam] = useState(1);

    const getChosenTeam = () => {
        const chosenTeamValue = betObj.chosenTeams[betObj?.players?.map(player => player.toLowerCase()).indexOf(account.toLowerCase())];
        if(chosenTeamValue == 1){
            return selectedGame.teams[0];
        } else if(chosenTeamValue == 2){
            return selectedGame.teams[1];
        } else if(chosenTeamValue == 3){
            return 'Draw';
        }
    }

    return (
        <Card sx={{ my: 2, p: 2, border: '1px solid #ccc' }}>
            <Typography variant="h5" component="div" sx={{ fontWeight: 'bold' }}>
                {title}
            </Typography>
            <Typography variant="body1" sx={{ mt: 1 }}>
                Bet amount: {betAmount} ETH
            </Typography>
            <Typography variant="body1">
                Total: {betObj?.players?.length || 0} Bets
            </Typography>
            <Typography variant="body1">
                {selectedGame.teams[0]}: { getCountPlayersByTeam(betObj, 1) || 0 } Bets
            </Typography>
            <Typography variant="body1">
                {selectedGame.teams[1]}: { getCountPlayersByTeam(betObj, 2) || 0 } Bets
            </Typography>
            <Typography variant="body1">
                Draw: { getCountPlayersByTeam(betObj, 3) || 0 } Bets
            </Typography>
            {
            betObj?.playerHasAlreadyJoined ?
                <Typography variant="body1" style={{color:"red"}}>
                    You have already joined this bet. (Chosen Team: {getChosenTeam()})
                </Typography>
            :
                <>
                    <br />
                    <div style={{ display: 'flex', alignItems: 'center', gap: '8px' }}>
                        <Select value={chosenTeam} onChange={(event) => setChosenTeam(event.target.value)} sx={{ height: '36px' }}>
                            <MenuItem value={1}>{selectedGame.teams[0]}</MenuItem>
                            <MenuItem value={2}>{selectedGame.teams[1]}</MenuItem>
                            <MenuItem value={3}>Draw</MenuItem>
                        </Select>
                        <Button variant="contained" onClick={() => joinBet(selectedGame.gameId, betAmount, chosenTeam)}>
                            Join Bet
                        </Button>
                    </div>
                </>
            }
        </Card>
    );
};

export default BetDetailBox;
