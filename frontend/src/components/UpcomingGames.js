import React from 'react';
import { Box, Card, Typography, Button } from '@mui/material';

const UpcomingGames = ({ upcomingGames, setSelectedGame }) => {
    return (
        <Box>
            {upcomingGames.map((game) => (
                <Card key={game.gameId} sx={{ my: 2, p: 2, border: '1px solid #ccc' }}>
                    <Typography variant="h6">
                        Game {game.gameId} - {game.teams[0]} vs {game.teams[1]}
                    </Typography>
                    <Typography variant="body1">
                        Scheduled Start Time: {new Date(game.startTime).toLocaleString()}
                    </Typography>
                    <Button variant="contained" onClick={() => setSelectedGame(game)}>
                        Show Bets
                    </Button>
                </Card>
            ))}
        </Box>
    );
};

export default UpcomingGames;
