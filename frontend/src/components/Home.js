import React, { useEffect, useState } from 'react';
import { ethers  } from 'ethers';
import web3 from 'web3';
import UpcomingGames from './UpcomingGames';
import ScoreGames from './ScoreGames';
import BetDetails from './BetDetails';
import { Tabs, Tab, Snackbar, Alert } from '@mui/material';
import { contractAbi } from '../utils/constants';

const Home = ({ contract, account }) => {

    const [upcomingGames, setUpcomingGames] = useState([]);
    const [scoreGames, setScoreGames] = useState([]);
    const [selectedGame, setSelectedGame] = useState(null);
    const [betsForSelectedGame, setBetsForSelectedGame] = useState([]);
    const [chosenGameIdForBet, setChosenGameIdForBet] = useState(0);
    const [tabValue, setTabValue] = useState(0);
    const [openSuccessAlert, setOpenSuccessAlert] = useState(false);
    const [successAlertMessage, setSuccessAlertMessage] = useState('');
    const [openErrorAlert, setOpenErrorAlert] = useState(false);
    const [errorAlertMessage, setErrorAlertMessage] = useState('');
    const [openInfoAlert, setOpenInfoAlert] = useState(false);
    const [infoAlertMessage, setInfoAlertMessage] = useState('');
    const [winnings, setWinnings] = useState(0);


    useEffect(() => {
        // fetch data from mock api
        const fetchUpcomingGames = async () => {
            try {
                const response = await fetch('https://65788dc3f08799dc80459625.mockapi.io/upcoming-games');
                const data = await response.json();
                setUpcomingGames(data);
                if(data.length > 0){
                    setChosenGameIdForBet(data[0].gameId);
                }
            } catch (error) {
                console.log(error);
            }
        };
        const fetchScoreGames = async () => {
            try {
                const response = await fetch('https://65788dc3f08799dc80459625.mockapi.io/score-games');
                const data = await response.json();
                setScoreGames(data);
            } catch (error) {
                console.log(error);
            }
        };
        fetchUpcomingGames();
        fetchScoreGames();
    }, []);

    useEffect(() => {
        if (selectedGame) {
            getBetsForSelectedGame()
        }
    }, [selectedGame]);

    useEffect(() => {
        if (contract && account) {
            const provider = new ethers.providers.Web3Provider(window.ethereum);
            const betFinishedFilter = contract.filters.BetFinished();
            const betFinishedListener = provider.on(betFinishedFilter, (event) => {
                const abi = contractAbi;
                const iface = new ethers.utils.Interface(abi);

                const decodedData = iface.decodeEventLog('BetFinished', event.data, event.topics);

                let gameId = parseInt(decodedData.gameId._hex);
                let winners = decodedData.winners;
                let winningAmount = parseInt(decodedData.totalWinningAmount._hex) / winners.length;

                if (winners.map(winner => winner.toLowerCase()).includes(account.toLowerCase())) {
                    console.log('Decoded Data:', decodedData);
                    console.log('BetFinished Event:', event);
                    setOpenSuccessAlert(true)
                    setSuccessAlertMessage("You have won " + (winningAmount / (Math.pow(10, 18))) + " ETH in game " + gameId.toString() + "!")
                    setWinnings(winnings + (winningAmount / (Math.pow(10, 18))))
                }
            });
        }
    }, [contract, account]);

    const getBetsForSelectedGame = async () => {
        if(contract && selectedGame){
            try {
                const betsArray = await contract.getBetsByGameId(selectedGame.gameId, {from: account});
                setBetsForSelectedGame(betsArray);
            } catch (error) {
                console.log(error);
            }
        }
    }

    const joinBet = async (gameId, betAmount, chosenTeam) => {
        if(contract){
            try {
                const provider = new ethers.providers.Web3Provider(window.ethereum);
                const signer = provider.getSigner();
                const connectedContract = contract.connect(signer);
                const transaction = await connectedContract.createOrJoinBet(gameId, chosenTeam, {
                    value: ethers.utils.parseEther(betAmount.toString()),
                    from: account,
                });
                setOpenInfoAlert(true)
                setInfoAlertMessage("Transaction in progress. Please wait")
                await transaction.wait();
                console.log('Transaction Hash:', transaction.hash);
                setOpenSuccessAlert(true)
                setSuccessAlertMessage("Bet placed successfully!")
            } catch (error) {
                setOpenErrorAlert(true)
                setErrorAlertMessage("Transaction failed")
            }
        }
    }

    const closeBetDetails = () => {
        setSelectedGame(null);
    }

    return (
        <>
            <h2 style={{display: 'flex', justifyContent: 'center'}}>
                Your Profit: <div style={{ color: 'green', marginLeft: '5px', marginRight: '5px'}}>{winnings}</div> ETH
            </h2>
            <Snackbar open={openSuccessAlert} autoHideDuration={3000} onClose={() => setOpenSuccessAlert(false)} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} >
                <Alert onClose={() => setOpenSuccessAlert(false)} severity="success" sx={{ width: '100%', padding: '20px' }}>
                    {successAlertMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={openInfoAlert} autoHideDuration={3000} onClose={() => setOpenInfoAlert(false)} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} >
                <Alert onClose={() => setOpenInfoAlert(false)} severity="info" sx={{ width: '100%', padding: '20px' }}>
                    {infoAlertMessage}
                </Alert>
            </Snackbar>
            <Snackbar open={openErrorAlert} autoHideDuration={3000} onClose={() => setOpenErrorAlert(false)} anchorOrigin={{ vertical: 'top', horizontal: 'right' }} >
                <Alert onClose={() => setOpenErrorAlert(false)} severity="error" sx={{ width: '100%', padding: '20px' }}>
                    {errorAlertMessage}
                </Alert>
            </Snackbar>
            <div style={{ display: 'flex', width:'100%'}}>
                <div style={{ flex: 1, padding: 10 }}>
                    <Tabs value={tabValue} onChange={(event, newValue) => setTabValue(newValue)}>
                        <Tab label="Upcoming Games" />
                        <Tab label="Game Scores" />
                    </Tabs>
                    {tabValue === 0 && <UpcomingGames upcomingGames={upcomingGames} setSelectedGame={setSelectedGame} />}
                    {tabValue === 1 && <ScoreGames scoreGames={scoreGames} setSelectedGame={setSelectedGame} />}
                </div>

                <div style={{ flex: 1, padding: 10 }}>
                    <BetDetails betsForSelectedGame={betsForSelectedGame} joinBet={joinBet} selectedGame={selectedGame} account={account} closeClicked={closeBetDetails} />
                </div>
            </div>
        </>
    );
};

export default Home;