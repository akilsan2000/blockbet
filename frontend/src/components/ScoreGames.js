import React from 'react';
import { Box, Card, Typography, Button } from '@mui/material';
const ScoreGames = ({ scoreGames, setSelectedGame }) => {
    return (
        <Box>
            {scoreGames.map((game) => (
                <Card key={game.gameId} sx={{ my: 2, p: 2, border: '1px solid #ccc' }}>
                    <Typography variant="h6">
                        Game {game.gameId} - {game.teams[0]} vs {game.teams[1]} ({game.score[game.teams[0]]} : {game.score[game.teams[1]]})
                    </Typography>
                    <Typography variant="body1">
                        {game.isFinished ? 'Game finished' : `Elapsed Time: ${game.elapsedTimeInMinutes} minutes`}
                    </Typography>
                    <Typography variant="body1">
                        Scheduled Start Time: {new Date(game.startTime * 1000).toLocaleString()}
                    </Typography>
                    {/* <Button variant="contained" onClick={() => setSelectedGame(game)}>
                        Show Bets
                    </Button> */}
                </Card>
            ))}
        </Box>
    );
};

export default ScoreGames;
