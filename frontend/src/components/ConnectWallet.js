import React, { useState, useEffect } from 'react';
import { ethers } from 'ethers';
import { Button, Typography, Tabs, Tab, Snackbar, Alert } from '@mui/material';

const ConnectWallet = ({ onAccountChanged }) => {
  const [account, setAccount] = useState(null);
  const [openSuccessAlert, setOpenSuccessAlert] = useState(false);
  const [successAlertMessage, setSuccessAlertMessage] = useState('');
  const [openErrorAlert, setOpenErrorAlert] = useState(false);
  const [errorAlertMessage, setErrorAlertMessage] = useState('');
  const [openInfoAlert, setOpenInfoAlert] = useState(false);
  const [infoAlertMessage, setInfoAlertMessage] = useState('');

  useEffect(() => {
    // Connect to MetaMask on component mount
    connectToMetaMask();
  }, []);

  const connectToMetaMask = async () => {
    if (window.ethereum) {
      try {
        // Request account access if needed
        await window.ethereum.request({ method: 'eth_requestAccounts' });

        // Get the current connected account
        const accounts = await window.ethereum.request({ method: 'eth_accounts' });
        setAccount(accounts[0]);
        onAccountChanged(accounts[0]);

        // Listen for account changes
        window.ethereum.on('accountsChanged', (newAccounts) => {
          setAccount(newAccounts[0]);
          onAccountChanged(newAccounts[0]);
        });
      } catch (error) {
        console.error('Error connecting to MetaMask:', error.message);
        setOpenErrorAlert(true)
        setErrorAlertMessage("Error connecting to MetaMask")
      }
    } else {
      setOpenInfoAlert(true)
      setInfoAlertMessage("MetaMask not detected. Please install MetaMask to use this feature.")
    }
  };

  return (
    <div>
      <Snackbar open={openSuccessAlert} autoHideDuration={3000} onClose={() => setOpenSuccessAlert(false)} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
          <Alert onClose={() => setOpenSuccessAlert(false)} severity="success" sx={{ width: '100%', padding: '20px' }}>
              {successAlertMessage}
          </Alert>
      </Snackbar>
      <Snackbar open={openInfoAlert} autoHideDuration={3000} onClose={() => setOpenInfoAlert(false)} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
          <Alert onClose={() => setOpenInfoAlert(false)} severity="info" sx={{ width: '100%', padding: '20px' }}>
              {infoAlertMessage}
          </Alert>
      </Snackbar>
      <Snackbar open={openErrorAlert} autoHideDuration={3000} onClose={() => setOpenErrorAlert(false)} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
          <Alert onClose={() => setOpenErrorAlert(false)} severity="error" sx={{ width: '100%', padding: '20px' }}>
              {errorAlertMessage}
          </Alert>
      </Snackbar>
      <div style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: '20px'}}>
      {account ? (
        <>
        <Typography sx={{padding:2}} variant="h5">Connected Account: </Typography>
        <Typography sx={{marginBottom: '20px', fontWeight: 'bold'}} variant="h5">{account}</Typography>
        </>
      ) : (
        <Button sx={{margin:2}}  variant="contained" onClick={connectToMetaMask}>
          Connect to MetaMask
        </Button>
      )}
      </div>
    </div>
  );
};

export default ConnectWallet;