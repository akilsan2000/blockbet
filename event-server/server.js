import express from 'express';
import Web3, { Web3Eth } from 'web3';

const app = express();
const port = 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
const jsonABI = [
  {
    "inputs": [
      {
        "internalType": "address",
        "name": "_oracle",
        "type": "address"
      },
      {
        "internalType": "bytes32",
        "name": "_jobId",
        "type": "bytes32"
      },
      {
        "internalType": "uint256",
        "name": "_fee",
        "type": "uint256"
      },
      {
        "internalType": "address",
        "name": "_link",
        "type": "address"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "constructor"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "bytes32",
        "name": "id",
        "type": "bytes32"
      }
    ],
    "name": "ChainlinkCancelled",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "bytes32",
        "name": "id",
        "type": "bytes32"
      }
    ],
    "name": "ChainlinkFulfilled",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": true,
        "internalType": "bytes32",
        "name": "id",
        "type": "bytes32"
      }
    ],
    "name": "ChainlinkRequested",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "isFinished",
        "type": "bool"
      }
    ],
    "name": "GameFinished",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "gameId",
        "type": "uint256"
      }
    ],
    "name": "GameID",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "bool",
        "name": "isFinished",
        "type": "bool"
      }
    ],
    "name": "GameNotFinished",
    "type": "event"
  },
  {
    "anonymous": false,
    "inputs": [
      {
        "indexed": false,
        "internalType": "uint256",
        "name": "gameWinner",
        "type": "uint256"
      }
    ],
    "name": "GameWinner",
    "type": "event"
  },
  {
    "inputs": [],
    "name": "bettingContractAddress",
    "outputs": [
      {
        "internalType": "address",
        "name": "",
        "type": "address"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bytes32",
        "name": "_requestId",
        "type": "bytes32"
      },
      {
        "internalType": "bool",
        "name": "_volume",
        "type": "bool"
      }
    ],
    "name": "fulfill",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bytes32",
        "name": "_requestId",
        "type": "bytes32"
      },
      {
        "internalType": "uint256",
        "name": "_volume",
        "type": "uint256"
      }
    ],
    "name": "fulfillGameId",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [
      {
        "internalType": "bytes32",
        "name": "_requestId",
        "type": "bytes32"
      },
      {
        "internalType": "uint256",
        "name": "_volume",
        "type": "uint256"
      }
    ],
    "name": "fulfillGameWinner",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "gameId",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "gameWinner",
    "outputs": [
      {
        "internalType": "uint256",
        "name": "",
        "type": "uint256"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "isFinished",
    "outputs": [
      {
        "internalType": "bool",
        "name": "",
        "type": "bool"
      }
    ],
    "stateMutability": "view",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "requestGameId",
    "outputs": [
      {
        "internalType": "bytes32",
        "name": "requestId",
        "type": "bytes32"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "requestGameWinner",
    "outputs": [
      {
        "internalType": "bytes32",
        "name": "requestId",
        "type": "bytes32"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "requestVolumeData",
    "outputs": [
      {
        "internalType": "bytes32",
        "name": "requestId",
        "type": "bytes32"
      }
    ],
    "stateMutability": "nonpayable",
    "type": "function"
  },
  {
    "inputs": [],
    "name": "withdrawLink",
    "outputs": [],
    "stateMutability": "nonpayable",
    "type": "function"
  }
]
var provider = "wss://sepolia.infura.io/ws/v3/48d61dda0547472986e7ef69b114b6c0";
var web3Provider = await new Web3.providers.WebsocketProvider(provider);
var web3 = await new Web3(web3Provider);
web3.eth.getBlockNumber().then((result) => {
  console.log("Latest Ethereum Block is ", result);
});
let myContract = await new web3.eth.Contract(jsonABI, "0x15A4EAbA080cF736A60d8C8A29eeaA582c555C9E") // 0x7F9979a96CA8B3043E815A9A351e1406BE7E49aD

console.log(myContract.eventNames)
console.log(myContract.events)

let options3 = {
  filter: {
      value: [],
  },
  fromBlock: 0
};
// Game Finished
myContract.events.GameFinished(options3)
  .on('data', event => console.log("GameFinished data",event.returnValues))

myContract.events.GameFinished(options3)
  .on('connected', str => console.log("GameFinished connected",str))

myContract.events.GameFinished(options3)
  .on('changed', str => console.log("GameFinished changed:",str))

myContract.events.GameFinished(options3)
  .on('error', str => console.log("GameFinished error",str))

myContract.events.GameNotFinished(options3)
  .on('data', event => console.log("GameFinished data",event.returnValues))

myContract.events.GameNotFinished(options3)
  .on('connected', str => console.log("connected",str))

myContract.events.GameNotFinished(options3)
  .on('changed', str => console.log("changed:",str))

myContract.events.GameNotFinished(options3)
  .on('error', str => console.log("error",str))


// Game id
myContract.events.GameID(options3)
  .on('data', event => console.log("GameID data",event.returnValues))

myContract.events.GameID(options3)
  .on('connected', str => console.log("GameID connected",str))

myContract.events.GameID(options3)
  .on('changed', str => console.log("GameID changed:",str))

myContract.events.GameID(options3)
  .on('error', str => console.log("GameID error",str))

// Game winner
myContract.events.GameWinner(options3)
  .on('data', event => console.log("GameWinner data",event.returnValues))

myContract.events.GameWinner(options3)
  .on('connected', str => console.log("GameWinner connected",str))

myContract.events.GameWinner(options3)
  .on('changed', str => console.log("GameWinner changed:",str))

myContract.events.GameWinner(options3)
  .on('error', str => console.log("GameWinner error",str))
