# BlockBet

## Prerequisites
- Node.js and npm installed
- Hardhat installed globally (`npm install -g hardhat`)

## Setup

### Install dependencies
```bash
npm install
cd frontend
npm install
```

### Clean and compile smart contract

```bash
npx hardhat clean
npx hardhat compile
```

### Deploy Betting Smart Contract to a specific network
```bash
npx hardhat run scripts/deploy.js --network sepolia
```

After the contract has been deployed, the console will show the address of the deployed BettingContract. Copy it into the variable contractAddress in /frontend/src/utils/constants.js

Also the generated artifact of the BettingContract needs to be copied into the frontend application:
```bash
cp build/artifacts/contracts/BettingContract.sol/BettingContract.json frontend/src/utils/BettingContract.json
```
**Note:** The copy step can be automated in the future but needs to be run at the moment.

### Deploy Automation and API Smart Contract to sepolia network
```bash
npm run deploy
```
Bei scripts/deployment/main.js muss jeweils der gewünschte Contract auskommentiert werden.
Am Besten deployed man zuerst den Betting Consumer Contract, danach muss diese Adresse im API Consumer gesetzt werden. Erst dann sollte der Api Consumer deployed werden. Diese deployte Adress muss in dem Automation Contract gesetzt werden. Zum Schluss kann dan der Automation Contract deployed werden. Mit dieser Adresse kann nun auf Chainlink ein Upkeep registriert werden.

**Note:** The copy step can be automated in the future but needs to be run at the moment.


## Environment Variables
Add the following environement variables in the .env file in the root directory:
```bash
PRIVATE_KEY=YOUR_WALLET_PRIVATE_KEY
SEPOLIA_RPC_URL='https://sepolia.infura.io/v3/your-api-key'
```


Add the following environement variables in the .env file in the /frontend/ directory:
```bash
REACT_APP_SEPOLIA_RPC_URL=https://sepolia.infura.io/v3/{YOUR_API_KEY}
```

## Run Frontend
```bash
cd frontend
npm start
```

## Testing
Unter dem Ordner Test gibt es zwei Ordner. Im Ordner Unit werden Tests auf dem Hardhat Netzwer ausgeführt. Dort wird das Chainlink Oracle gemockt. Im Ornder Staging werden die Tests auf Sepolia deployet und getestet. Dabei wird ein Chainlink Testnet Oracle verwendet.

Für die Test im Unit Ordner:
```bash
npm run test 
```
Für die Test im Staging Ordner:
```bash
npm run test-staging
```


